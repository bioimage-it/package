# This repository is obsolete, the BioImageIT project has been move to https://github.com/bioimageit

# Package

This is a package script that install all the BioImageIT tools in one directory.

This script does not install backend tools (Docker or Singularity or Omero or ... ) that are specific to an install

We recommend using conda (or anaconda) to manage the python dependencies

1- To create the package, just run the package.sh script:

```
./package.sh
```

2- To install the package, run the command

```
cd bioimageit # or the path of the package is you moved it
./install.sh
```

## move package

The package contains a config file `config.json` with absolute configurations path.
If you move the package directory, you need to change the paths in `config.json` or run the command

```
cd /path/of/the/package/dir/
python3 bioimagepy/install.py "your user name"
```