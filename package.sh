#!/bin/bash

mkdir -p bioimageit
cd bioimageit

# user data
mkdir -p userdata

# bioimagepy
git clone https://gitlab.inria.fr/bioimage-it/bioimagepy.git

# bioimageapp
git clone https://gitlab.inria.fr/bioimage-it/bioimageapp.git

# toolboxes
git clone https://gitlab.inria.fr/bioimage-it/toolboxes.git

# allgo
git clone https://gitlab.inria.fr/allgo/api-clients/python_client.git
git checkout client_api

# start scipts
cp -a ../scripts/. ./

# tutorials
git clone https://gitlab.inria.fr/bioimage-it/bioimagepy-tutorial.git

