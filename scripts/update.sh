#!/bin/bash


BASEDIR=$(dirname $0)
cd $BASEDIR

# bioimagepy
cd bioimagepy
git pull origin master
cd ..

# bioimageapp
cd bioimageapp
git pull origin master
cd ..

# toolboxes
cd toolboxes
git pull origin master
cd ..

# tutorials
cd bioimagepy-tutorial
git pull origin master
cd ..

# allgo
cd python_client
git pull origin client_api
cd ..

# configure bioimagepy 
source .bioimageit-env/bin/activate
python3 -m pip install -e bioimagepy
python3 -m pip install -e bioimageapp

# build toolboxes
python3 bioimagepy/toolboxes.py

