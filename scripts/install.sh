#!/bin/bash

BASEDIR=$(dirname $0)
cd $BASEDIR

echo 
read -p 'Please enter a username:' username
echo The username: "${username}" will be used to identify you as the author in the metadata. You can change it in the config.json file

# python env 
python3 -m venv .bioimageit-env
source .bioimageit-env/bin/activate

# install allgo
cd python_client
git checkout client_api
cd ..
python3 -m pip install requests
python3 -m pip install -e python_client

# install and config bioimageapy and bioimage app
python3 -m pip install -e bioimagepy
python3 -m pip install -e bioimageapp
python3 bioimagepy/config.py "${username}"
python3 bioimageapp/config.py

# install jupyter
python3 -m pip install imageio
python3 -m pip install matplotlib
python3 -m pip install jupyter
