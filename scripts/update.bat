cd %cd%
call .bioimageit-env\Scripts\activate.bat

REM bioimagepy
cd bioimagepy
git pull origin master
cd ..

REM bioimageapp
cd bioimageapp
git pull origin master
cd ..

REM toolboxes
cd toolboxes
git pull origin master
cd ..

REM tutorials
cd bioimagepy-tutorial
git pull origin master
cd ..

REM allgo
cd python_client
git pull origin client_api
cd ..

REM configure bioimagepy 
python3 -m pip install -e bioimagepy
python3 -m pip install -e bioimageapp

REM build toolboxes
python3 bioimagepy\toolboxes.py

