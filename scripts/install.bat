cd %cd%


REM python env 
python3 -m venv .bioimageit-env
call .bioimageit-env\Scripts\activate.bat

REM install allgo
python3 -m pip install -e python_client

REM install and config bioimageapy and bioimage app
python3 -m pip install -e bioimagepy
python3 -m pip install -e bioimageapp
python3 bioimagepy/config.py "unknown"
python3 bioimageapp/config.py

REM install jupyter
python3 -m pip install imageio
python3 -m pip install matplotlib
python3 -m pip install jupyter
